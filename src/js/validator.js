$( function( $ ) {

	$( '#form' ).on( 'submit', function( event ) {
		if ( validateForm() ) {

			// если есть ошибки возвращает true
			event.preventDefault();
		} else {
			event.preventDefault();
			var that = $( this );
			$.ajax({
				type: 'POST',
				url: that.attr( 'action' ),
				data: that.serialize(),
				success: function() {
					$( '#small-modal' ).modal( 'show' );
					$( '#form' )
						.find( '.form-control' )
						.removeClass( 'is-invalid' );
					that[ 0 ].reset();
				}
			});
		}
	});

	function validateForm() {
		$( '.text-error' ).remove();

		// Проверка логина
		var $login = $( '#name' );
		if ( $login.val().length < 2 ) {
			var loginValue = true;
			$login.addClass( 'is-invalid' );
		}
		$( '#login' ).toggleClass( 'is-invalid', loginValue );


		var reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		var $email = $( '#email' );
		var emailValue = $email.val() ? false : true;

		if ( emailValue ) {
			$email.addClass( 'is-invalid' );
		} else if ( !reg.test( $email.val() ) ) {
			emailValue = true;
			$email.addClass( 'is-invalid' );
		}
		$( '#email' ).toggleClass( 'is-invalid', emailValue );
		return ( loginValue || emailValue );
	}
});
